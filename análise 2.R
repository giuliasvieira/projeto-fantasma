#####_______________________Projeto Fantasma_______________________#####
####______________________Giulia Santos Vieira______________________####

#Carregando os pacotes

if(!require(openxlsx)){install.packages("openxlsx");library(openxlsx)}
if(!require(tidyverse)){install.packages("tidyverse");library(tidyverse)}
if(!require(ggplot2)){install.packages("ggplot2");library(ggplot2)}
if(!require(dplyr)){install.packages("dplyr");library(dplyr)}
if(!require(tidyr)){install.packages("tidyr");library(tidyr)}

#ESTAT

cores_estat <- c('#A11D21','#663333','#FF6600','#CC9900','#CC9966','#999966','#006606','#008091','#003366','#041835','#666666')

theme_estat <- function(...) {
  theme <- ggplot2::theme_bw() +
    ggplot2::theme(
      axis.title.y = ggplot2::element_text(colour = "black", size = 12),
      axis.title.x = ggplot2::element_text(colour = "black", size = 12),
      axis.text = ggplot2::element_text(colour = "black", size = 9.5),
      panel.border = ggplot2::element_blank(),
      axis.line = ggplot2::element_line(colour = "black"),
      legend.position = "top",
      ...
    )
  
  return(
    list(
      theme,
      scale_fill_manual(values = cores_estat),
      scale_colour_manual(values = cores_estat)
    )
  )
}

#Importando banco de dados

dados <- read.xlsx("dados_PS.xlsx",
                   startRow = 4,
                   colNames = TRUE,
                   rowNames = FALSE)

dados

####Relação significativa entre a idade das marcas e a avaliação de ousadia####

summary(dados$Idade)
summary(dados$`Q7_1.-.Daring`) 

#Renomeando a variável

dados <- rename(dados, Ousadia = `Q7_1.-.Daring`)

summary(dados$Ousadia)

#Retirando os missing values(NA)

dados <- dados[!is.na(dados$Ousadia),]

#Plotando o gráfico de dispersão

ggplot(dados) +
  aes(x = Ousadia, y = Age) +
  geom_point(colour = "#A11D21", size = 1.2) +
  labs(
    x = "Avaliação de ousadia",
    y = "Idade das marcas (anos)"
  ) +
  theme_estat() +
  scale_x_continuous(
    breaks = c(1,2,3,4,5,6)
  ) +
  scale_y_continuous(breaks = c(seq(0,210,30)))

ggsave("graf_an2.pdf", width = 158, height = 93, units = "mm")

#Calculando o coeficiente de correlação de Pearson

cor.test(
  x = dados$Age, 
  y = dados$Ousadia, 
  method = "pearson"
)