#####_______________________Projeto Fantasma_______________________#####
####______________________Giulia Santos Vieira______________________####

#Carregando os pacotes

if(!require(openxlsx)){install.packages("openxlsx");library(openxlsx)}
if(!require(tidyverse)){install.packages("tidyverse");library(tidyverse)}
if(!require(ggplot2)){install.packages("ggplot2");library(ggplot2)}
if(!require(dplyr)){install.packages("dplyr");library(dplyr)}
if(!require(tidyr)){install.packages("tidyr");library(tidyr)}
if(!require(scales)){install.packages("scales");library(scales)}


#ESTAT

cores_estat <- c('#A11D21','#663333','#FF6600','#CC9900','#CC9966','#999966','#006606','#008091','#003366','#041835','#666666')

theme_estat <- function(...) {
  theme <- ggplot2::theme_bw() +
    ggplot2::theme(
      axis.title.y = ggplot2::element_text(colour = "black", size = 12),
      axis.title.x = ggplot2::element_text(colour = "black", size = 12),
      axis.text = ggplot2::element_text(colour = "black", size = 9.5),
      panel.border = ggplot2::element_blank(),
      axis.line = ggplot2::element_line(colour = "black"),
      legend.position = "top",
      ...
    )
  
  return(
    list(
      theme,
      scale_fill_manual(values = cores_estat),
      scale_colour_manual(values = cores_estat)
    )
  )
}

#Importando banco de dados

dados <- read.xlsx("dados_PS.xlsx",
                   startRow = 4,
                   colNames = TRUE,
                   rowNames = FALSE)

dados

#####Relação entre risco em caso de decisão errada e importância da decisão####

summary(dados$`Q4:.Risk.in.case.of.wrong.decision`)
summary(dados$`Q2:.importance.of.decision`)

#Gráfico

ggplot(dados) +
  aes(x = `Q4:.Risk.in.case.of.wrong.decision`, y = `Q2:.importance.of.decision`) +
  geom_point(colour = "#A11D21", size = 2) +
  labs(
    x = "Risco em caso de decisão errada",
    y = "Importância da decisão"
  ) +
  theme_estat()

ggsave("graf_an4.pdf", width = 158, height = 93, units = "mm")

#Calculando o coeficiente de correlação

cor.test(
  x = dados$`Q4:.Risk.in.case.of.wrong.decision`, 
  y = dados$`Q2:.importance.of.decision`, 
  method = "pearson"
)
